.. include:: global.rst.inc


Mission Support System's Documentation
========================================================

.. image:: https://anaconda.org/conda-forge/mss/badges/version.svg

.. include:: ../README


.. toctree::
   :maxdepth: 2

   about
   dependencies
   installation
   components
   development
   authors


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

